'''Functions to calculate Newtonian noise

'''
from __future__ import division
import numpy as np
from numpy import pi, sqrt, exp, log10, sin
import scipy.integrate as scint
import scipy.special as sp
from scipy.interpolate import interp1d
import os

from .seismic import seismic_ground_NLNM
from .. import const


def gravg(f, seismic):
    """Gravity gradient noise for single test mass

    :f: frequency array in Hz
    :seismic: gwinc Seismic struct

    :returns: displacement noise power spectrum at :f:, in meters

    References:

     Saulson 1984,           http://dx.doi.org/10.1103/PhysRevD.30.732
     Hughes and Thorne 1998, http://dx.doi.org/10.1103/PhysRevD.58.122002

     Driggers and Harms 2011, ``Results of Phase 1 Newtonian Noise
     Measurements at the LIGO Sites,'' February-March 2011.  T1100237.
     https://dcc.ligo.org/LIGO-T1100237

    Written by Enrico Camagna (?)

    added to Bench by Gregg Harry 8/27/03
    seismic spectrum modified by Jan Harms 05/11/2010
    Calculates gravity gradient noise for four mirrors

    """

    fk = seismic.KneeFrequency
    a = seismic.LowFrequencyLevel
    gamma = seismic.Gamma
    rho = seismic.Rho
    # factor to account for correlation between masses
    # and the height of the mirror above the ground
    beta = seismic.Beta
    h = seismic.TestMassHeight
    c_rayleigh = seismic.RayleighWaveSpeed

    if 'Omicron' in seismic:
        omicron = seismic.Omicron
    else:
        omicron = 1

    # a sort of theta function (Fermi distr.)
    coeff = 3**(-gamma*f)/(3**(-gamma*f) + 3**(-gamma*fk))

    # modelization of seismic noise (vertical)
    ground = a*coeff + a*(1-coeff)*(fk/f)**2
    if 'Site' in seismic and seismic.Site == 'LLO':
        ground = a*coeff*(fk/f) + a*(1-coeff)*(fk/f)**2

    # effective GG spring frequency, with G gravitational
    fgg = sqrt(const.G * rho) / (2*pi)

    # fixed numerical factors, 5/9/06, PF
    n = (beta*2*pi*(fgg**2/f**2)*ground)**2

    # The following two terms are corrections due to Jan Harms
    # https://git.ligo.org/rana-adhikari/CryogenicLIGO/issues/45
    # (1) projection of NN force onto the direction of the arm
    n = n * 1/2
    # (2) exponential cutoff at frequency (seismic speed)/(test mass height)
    n = n * exp(-4*pi*f*h/c_rayleigh)

    # Feedforward cancellation
    n /= (omicron**2)

    return n


def gravg_rayleigh(f, seismic):
    """Gravity gradient noise for single arm cavity from seismic Rayleigh waves

    :f: frequency array in Hz
    :seismic: gwinc Seismic structure

    :returns: displacement noise power spectrum at :f:, in meters

    Following Harms LRR: https://doi.org/10.1007/lrr-2015-3
    and Amann et al.: https://doi.org/10.1063/5.0018414

    """
    fk = seismic.KneeFrequency
    a = seismic.LowFrequencyLevel
    gamma = seismic.Gamma
    rho = seismic.Rho
    h = seismic.TestMassHeight
    c_rayleigh = seismic.RayleighWaveSpeed

    if 'Omicron' in seismic:
        omicron = seismic.Omicron
    else:
        omicron = 1

    # a sort of theta function (Fermi distr.)
    coeff = 3**(-gamma*f)/(3**(-gamma*f) + 3**(-gamma*fk))

    # modelization of seismic noise (vertical)
    ground = a*coeff + a*(1-coeff)*(fk/f)**2
    if 'Site' in seismic and seismic.Site == 'LLO':
        ground = a*coeff*(fk/f) + a*(1-coeff)*(fk/f)**2

    # Harms LRR eqs. 35, 96, and 98
    w = 2 * pi * f
    k = w / c_rayleigh
    kP = w / seismic.pWaveSpeed
    kS = w / seismic.sWaveSpeed
    qzP = sqrt(k**2 - kP**2)
    qzS = sqrt(k**2 - kS**2)
    zeta = sqrt(qzP / qzS)

    gnu = k * (1 - zeta) / (qzP - k * zeta)
    
    if h >= 0:
        # Harms LRR
        n = (2 * pi * const.G * rho * exp(-h * k) * gnu)**2 * ground**2 / w**4

    else:
        # Amann et al., eqs. 2-6. Note h is there defined as depth;
        # We define it as height.
        r0 = k * (1 - zeta)
        sh = -k * (1 + zeta) * exp(h * k)
        bh = (2 / 3) * (2 * k * exp(h * qzP) + zeta * qzS * exp(h * qzS))
        Rcal = np.abs((sh + bh) / r0)**2
        n = 4 * (sqrt(2) * pi * const.G * rho * gnu)**2 * Rcal * ground**2 / w**4

    n /= omicron**2

    return n


def gravg_pwave(f, seismic, exact=False):
    """Gravity gradient noise for single test mass from seismic p-waves

    :f: frequency array in Hz
    :seismic: gwinc Seismic structure
    :exact: whether to use a slower numerical integral good to higher frequencies
            or faster special functions. If exact=False, nan is returned at
            high frequencies where the special functions have numerical errors.

    :returns: displacement noise power spectrum at :f:, in meters

    Following Harms LRR: https://doi.org/10.1007/lrr-2015-3

    """
    cP = seismic.pWaveSpeed
    levelP = seismic.pWaveLevel
    tmheight = seismic.TestMassHeight
    rho_ground = seismic.Rho

    kP = (2 * pi * f) / cP

    psd_ground_pwave = (levelP * seismic_ground_NLNM(f))**2

    xP = np.abs(kP * tmheight)

    if tmheight >= 0:
        # Surface facility
        # The P-S conversion at the surface is not implemented
        if exact:
            height_supp_power = (3 / 2) * np.array(
                [scint.quad(lambda th, x: np.sin(th)**3
                            * np.exp(-2 * x * np.sin(th)), 0, pi / 2, args=(x,))[0]
                 for x in xP])

        else:
            xP[xP > 10] = np.nan
            height_supp_power = 3 / (24*xP) * (
                8*xP - 9*pi*sp.iv(2, 2*xP) - 6*pi*xP*sp.iv(3, 2*xP)
                + 6*pi*xP*sp.modstruve(1, 2*xP) - 3*pi*sp.modstruve(2, 2*xP))

    else:
        # Underground facility
        # The cavity effect is not included
        if exact:
            height_supp_power = (3 / 4) * np.array(
                [scint.quad(lambda th, x: np.sin(th)**3
                            * (2 - np.exp(-x * np.sin(th)))**2, 0, pi, args=(x,))[0]
                 for x in xP])

        else:
            xP[xP > 2] = np.nan
            height_supp_power = 1 + 3*pi/(8*xP) * (
                24*sp.iv(2, xP) - 3*sp.iv(2, 2*xP) + 8*xP*sp.iv(3, xP)
                - 2*xP*sp.iv(3, 2*xP) - 8*xP*sp.modstruve(1, xP)
                + 2*xP*sp.modstruve(1, 2*xP) - 8*sp.modstruve(2, xP)
                + sp.modstruve(2, 2*xP))

    psd_gravg_pwave = ((2 * pi * const.G * rho_ground)**2
            * psd_ground_pwave * height_supp_power)
    psd_gravg_pwave /= (2 * pi * f)**4
    return psd_gravg_pwave


def gravg_swave(f, seismic, exact=False):
    """Gravity gradient noise for single test mass from seismic s-waves

    :f: frequency array in Hz
    :seismic: gwinc Seismic structure
    :exact: whether to use a slower numerical integral good to higher frequencies
            or faster special functions. If exact=False, nan is returned at
            high frequencies where the special functions have numerical errors.

    :returns: displacement noise power spectrum at :f:, in meters

    Following Harms LRR: https://doi.org/10.1007/lrr-2015-3

    """
    cS = seismic.sWaveSpeed
    levelS = seismic.sWaveLevel
    tmheight = seismic.TestMassHeight
    rho_ground = seismic.Rho

    kS = (2 * pi * f) / cS

    psd_ground_swave = (levelS * seismic_ground_NLNM(f))**2

    xS = np.abs(kS * tmheight)

    # For both surface and underground facilities
    if exact:
        height_supp_power = (3 / 2) * np.array(
            [scint.quad(lambda th, x: np.sin(th)**3
                        * np.exp(-2 * x * np.sin(th)), 0, pi / 2, args=(x,))[0]
             for x in xS])

    else:
        xS[xS > 10] = np.nan
        height_supp_power = 3 / (24*xS) * (
            8*xS - 9*pi*sp.iv(2, 2*xS) - 6*pi*xS*sp.iv(3, 2*xS)
            + 6*pi*xS*sp.modstruve(1, 2*xS) - 3*pi*sp.modstruve(2, 2*xS))

    psd_gravg_swave = ((2 * pi * const.G * rho_ground)**2
            * psd_ground_swave * height_supp_power)
    psd_gravg_swave /= (2 * pi * f)**4

    return psd_gravg_swave


def atmois(f, atmos, seismic):
    """Atmospheric infrasound newtonian noise for single arm cavity

    :f: frequency array in Hz
    :atmos: gwinc Atmospheric structure
    :seismic: gwinc Seismic structure

    :returns: displacement noise power spectrum at :f:, in meters

    """
    p_air = atmos.AirPressure
    rho_air = atmos.AirDensity
    ai_air = atmos.AdiabaticIndex
    c_sound = atmos.SoundSpeed
    h = seismic.TestMassHeight

    w = 2 * pi * f
    k = w / c_sound

    # Pressure spectrum
    try:
        a_if = atmos.InfrasoundLevel1Hz
        e_if = atmos.InfrasoundExponent
        psd_if = (a_if * f**e_if)**2
    except AttributeError:
        psd_if = atmoBowman(f)**2

    # Harms LRR (2015), eq. 172
    # https://doi.org/10.1007/lrr-2015-3
    # And with the Bessel terms ignored... for 4 km this amounts to a 10%
    # correction at 10 Hz and a 30% correction at 1 Hz
    coupling_if = 2/3 * (4 * pi / (k * w**2) * const.G * rho_air / (ai_air * p_air))**2

    n_if = coupling_if * psd_if

    return n_if


def advectedTemp(f, atmos, infrastructure):
    """Gravity gradient noise for full interferometer from advected temperature
    fluctuations.

    :f: frequency array in Hz
    :atmos: gwinc Atmospheric structure
    :infrastructure: gwinc Infrastructure structure

    :returns: displacement noise power spectrum at :f:, in meters

    Following Harms LRR:  https://doi.org/10.1007/s41114-019-0022-2

    """

    rho_air = atmos.AirDensity
    T_air = atmos.Temperature
    cT = atmos.TempStructConst
    p = atmos.TempStructExp
    v = atmos.WindSpeed
    rmin = infrastructure.BuildingRadius

    w = 2 * pi * f

    n_at = ((2 * pi)**3 * (const.G * rho_air * cT / T_air)**2
             * w**-(p + 7) * sp.gamma(2 + p) * sin(pi * p / 2)
             * exp(-2 * rmin * w / v) * v**(p + 2))
    
    return n_at


def aeroacoustic(f, atmos):
    """Gravity gradient noise for a single arm from aeroacoustic (Lighthill) noise

    :f: frequency array in Hz
    :atmos: gwinc Atmospheric structure

    :returns: displacement noise power spectrum at :f:, in meters

    Following Sec. 5.4 of Harms (2019): https://doi.org/10.1007/s41114-019-0022-2

    """

    p_air = atmos.AirPressure
    nu = atmos.AirKinematicViscosity
    c_sound = atmos.SoundSpeed
    lTurb = atmos.TurbOuterScale
    K0 = 0.53  # Kolmogorov constant
    # eps = atmos.TurbEnergyDissRate
    eps = (atmos.KolmEnergy1m / K0)**(3/2)


    # Length scales for inertial subrange
    k0 = 2 * pi / lTurb
    knu = (eps / nu**3)**(1/4)

    # load the precomputed integrals in Harms 159
    fname = os.path.join(os.path.dirname(__file__), 'aeroacoustic_integral.txt')
    Ctot_interp = interp1d(*np.loadtxt(fname), bounds_error=True)
    kf, Cf = Ctot_interp.x[-1], Ctot_interp.y[-1]

    # at large frequencies approximate the integrals as a power law
    def Ctot_piecewise(k):
        if k <= kf:
            return Ctot_interp(k)
        else:
            return Cf * (kf / k)**(4/3)

    Ctot = np.vectorize(Ctot_piecewise)


    def Sak(k, w):
        """
        Diagonal component of acceleration PSD over both f and (kx, ky, kz)

        Harms Eq. 159
        """
        # Eddy turnover times
        v = (eps / k)**(1/3)
        tau0 = 1 / (k * v)

        Sak_num = (2 * const.G * p_air / c_sound**2)**2 * tau0 / pi**(3/2)
        Sak_den = k0**(4/3) * k * (w**2 - c_sound**2 * k**2)**2
        C = K0**2 * eps**(4/3) * Ctot(k/k0)

        return Sak_num/Sak_den * exp(-w**2 * tau0**2 / 4) * C

    # Integrate over 3D grid of k-vectors to get PSD over f only
    Sa = np.array(
        [scint.quad(lambda k: 4*pi*k**2 * Sak(k, w), k0, knu)[0] for w in 2*pi*f])

    # convert to displacement
    return Sa / (2*pi*f)**4


def atmoBowman(f):
    """The Bowman infrasound model
   
    """ 
    freq = np.array([
        0.01, 0.0155, 0.0239, 0.0367, 0.0567,
        0.0874, 0.1345, 0.2075, 0.32, 0.5,
        0.76, 1.17, 1.8, 2.79, 4.3,
        6.64, 10, 100,
    ])
    pressure_asd = sqrt([
        22.8, 4, 0.7, 0.14, 0.027, 0.004,
        0.0029, 0.0039, 7e-4, 1.44e-4, 0.37e-4,
        0.12e-4, 0.56e-5, 0.35e-5, 0.26e-5, 0.24e-5,
        2e-6, 2e-6,
    ])
    return 10**(np.interp(log10(f), log10(freq), log10(pressure_asd)))


def generate_aeroacoustic_integral(npts, fname='aeroacoustic_integral.txt'):
    """Computes the integrals in Harms Eq. 159

    :npts: number of log spaced points between 1 and 100 to compute
    :fname: file name to save the data to
    """
    import mpmath as mp
    from hankel import HankelTransform

    def kolm0_int(r):
        """Compute the dimensionless integral

        kolm0 = \int_1^\infty dk k^{-5/3} j_0(kr)
        """
        kolm0 = -r**(2/3) / 2 * mp.gamma(-5/3)
        kolm0 += 3/2 * mp.hyper([-1/3], [2/3, 3/2], -r**2 / 4)
        return float(kolm0)

    def kolm2_int(r):
        """Compute the dimensionless integral

        kolm2 = \int_1^\infty dk k^{-5/3} j_2(kr)
        """
        kolm2 = 27*r**(2/3) / 110 * mp.gamma(4/3)
        kolm2 -= r**2 / 20 * mp.hyper([2/3], [5/3, 7/2], -r**2 / 4)
        return float(kolm2)

    # vectorized Kolmogorov integrals
    kolm0_vec = np.vectorize(kolm0_int)
    kolm2_vec = np.vectorize(kolm2_int)

    # HankelTransformation classes for spherical Bessel functions of order n
    # Note that to get the spherical Bessel functions, the kernel must be
    # multiplied by sqrt(pi/(2*z)) which is done in the kernel functions below
    ht0 = HankelTransform(nu=1/2)
    ht2 = HankelTransform(nu=5/2)
    ht4 = HankelTransform(nu=9/2)

    def kern0(k, r):
        """The kernel for the 0th order Bessel function
        """
        kolm0 = kolm0_vec(r/k)
        kolm2 = kolm2_vec(r/k)
        return r**2 * np.sqrt(np.pi/(2*r)) * (4/9 * kolm0**2 + 4/45 * kolm2**2)

    def kern2(k, r):
        """The kernel for the 2nd order Bessel function
        """
        kolm0 = kolm0_vec(r/k)
        kolm2 = kolm2_vec(r/k)
        return -8 * r**2 * np.sqrt(np.pi/(2*r)) * (kolm0*kolm2 / 9 + kolm2**2 / 63)

    def kern4(k, r):
        """The kernel for the 4th order Bessel function
        """
        kolm2 = kolm2_vec(r/k)
        return 8/35 * r**2 * np.sqrt(np.pi/(2*r)) * kolm2**2

    def coeff(ht, kernel, k):
        """Compute one of the coefficients

        ht: the appropriate Hankel transform
        kernel: the appropriate kernel
        k: the dimensionless wavevector
        """
        return ht.integrate(lambda r: kernel(k, r))[0]

    # sum of the 0th, 2nd, and 4th order coefficients
    Ctot_vec = np.vectorize(
        lambda x: coeff(ht0, kern0, x) + coeff(ht2, kern2, x) + coeff(ht4, kern4, x))

    kk = np.logspace(0, 2, npts)
    Ctot = Ctot_vec(kk)
    np.savetxt(fname, np.vstack((kk, Ctot)))
